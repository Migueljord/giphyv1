/**
 * Esta ruta traera todas las busquedas que se hagan por defecto desde la vista inicial.
 * @param {string} nombre del componente de donde sera llamada esta funcion 
 * @param {*} filtro 
 */

async function API_getgif(componente, filtro){
    console.log("API_getgif()")
    const APIkey="d7xdlYBmGeKOMk8OAgLYYVlHbN6iX374"
    const limit = 4
    let url =''

    if (componente == 'trending') {
        url = `http://api.giphy.com/v1/gifs/trending?api_key=${APIkey}&limit=${limit}&rating=g`
    }
    if(componente == 'random'){
        url = `http://api.giphy.com/v1/gifs/random?api_key=${APIkey}&tag=${filtro}`
    }
    if (componente == 'artistas'){
        url = `http://api.giphy.com/v1/gifs/search?api_key=${APIkey}&q=${filtro}&limit=${limit}`
    }
    if (componente == 'busqueda'){
        url = `http://api.giphy.com/v1/gifs/search?api_key=${APIkey}&q=${filtro}&limit=${limit}`
    }
    const respuesta = await axios.get(url)
    respuesta = await response.data.data.json()
    console.log(respuesta)
    return respuesta
}


