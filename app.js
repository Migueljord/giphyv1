const app = new Vue({
    el: "#app",
    data(){
        return{
            mensaje: "Hola desde Vuejs",
            filtro:'burrito',
            listaTrendin :[],
            listaRandom: [],
            listaBusqueda:[],
            listaArtistas:[],
            listaCategorias:[],
            APIkey: "d7xdlYBmGeKOMk8OAgLYYVlHbN6iX374",
            limit : 4,
            paginaTrendin :0,
            paginaRandom :0,
            paginaBusqueda :0,
            paginaArtistas :0,
        }
    },
    methods: {
       async getTrendins(){
            const arr = []
            url = `http://api.giphy.com/v1/gifs/trending?api_key=${this.APIkey}&limit=${this.limit}&rating=g&offset=${this.paginaTrendin}`
            const respuesta = await axios.get(url)
            respuesta.data.data.forEach(gif => {
                arr.push({
                    "id": gif.id,
                    "import_datetime":gif.import_datetime,
                    "url":gif.images.original.url,
                    "height":gif.images.original.height,
                    "width":gif.images.original.width,
                    "mp4":gif.images.original.mp4,
                    "title":gif.title
                })
            });
            return arr
        },
      async  getRandom(){
            const arr = []
            url = `http://api.giphy.com/v1/gifs/random?api_key=${this.APIkey}&tag=${this.filtro}`
            const respuesta = await axios.get(url)

            respuesta.data.data.forEach(gif => {
                arr.push({
                    "id": gif.id,
                    "import_datetime":gif.import_datetime,
                    "url":gif.images.original.url,
                    "height":gif.images.original.height,
                    "width":gif.images.original.width,
                    "mp4":gif.images.original.mp4,
                    "title":gif.title
                })
            });
            return arr
        },
        async getArtistas(){
            const arr = []
            url = `http://api.giphy.com/v1/gifs/search?api_key=${this.APIkey}&q="Artistas"&limit=${this.limit}&offset=${this.paginaArtistas}`
            const respuesta = await axios.get(url)
            respuesta.data.data.forEach(gif => {
                arr.push({
                    "id": gif.id,
                    "import_datetime":gif.import_datetime,
                    "url":gif.images.original.url,
                    "height":gif.images.original.height,
                    "width":gif.images.original.width,
                    "mp4":gif.images.original.mp4,
                    "title":gif.title
                })
            });
            return arr           
        },           
        async getCategorias(){
            const arr = []
            url = `http://api.giphy.com/v1/gifs/categories?api_key=${this.APIkey}`
            try {
                const respuesta = await axios.get(url)
                console.log(respuesta)
                respuesta.data.data.forEach(categoria =>{
                    arr.push({
                        "id":categoria.gif.id,
                        "name":categoria.name,
                        "url":categoria.gif.url
                    })
                })
                return arr               
            } catch (error) {
                console.error("Este es el error:"+error)
            }
        },
        async  siguienteTrending(){
            this.paginaTrendin+=5
            this.listaTrendin = await this.getTrendins()
        },
        async  siguienteArtistas(){
            this.paginaArtistas+=5
            this.listaArtistas = await this.getArtistas()
        }
    },
   async created() {
    this.paginaTrendin = 0,
    this.paginaBusqueda = 0,
    this.paginaArtistas = 0,
        this.listaCategorias = await this.getCategorias()
        this.listaTrendin = await this.getTrendins()
        //this.listabusqueda = this.getBusqueda()
        this.listaArtistas = await this.getArtistas()
    },
})